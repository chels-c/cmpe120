import string


def uppercase(letter):
    lowerLetters = list(string.ascii_lowercase);
    upperLetters = list(string.ascii_uppercase);
    for i in range(len(lowerLetters)):
       if letter == lowerLetters[i]:
            return upperLetters[i]
    return letter

def lowercase(letter):
    lowerLetters = list(string.ascii_lowercase);
    upperLetters = list(string.ascii_uppercase);
    for i in range(len(upperLetters)):
        if letter == upperLetters[i]:
            return lowerLetters[i]
    return letter

def isLetter(letter):
    lowerLetters = list(string.ascii_lowercase);
    upperLetters = list(string.ascii_uppercase);
    for i in range(len(upperLetters)):
        if letter == upperLetters[i] or letter == lowerLetters[i]:
            return True
    return False

def isDigit(digit):
    strDigit = str(digit)
    numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    for i in range(len(strDigit)):
        for j in range(len(numbers)):
            if strDigit[i] == numbers[j]:
                return True
    return False

def isSpecialCharacter(char):
    strChar = str(char)
    for i in range(len(strChar)):
        if not isDigit(strChar[i]) or not isLetter(strChar[i]):
            return True
    return False

print(uppercase("g"))
print(lowercase("B"))
print(isLetter("h"))
print(isLetter("#"))
print(isDigit(3))
print(isDigit(133))
print(isSpecialCharacter("&"))





